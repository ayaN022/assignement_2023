package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DepositMapperTest {

    @Test
    public void testToDepositDTO() {
        MoneyDeposit deposit = new MoneyDeposit();
        DepositMapper mapper = new DepositMapper();
        DepositDto dto = mapper.toDepositDto(deposit);
        assertEquals(dto.getMontant(), deposit.getMontant());



    }
}
