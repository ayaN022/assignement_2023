package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;

public class TransferMapper {


    public static TransferDto map(Transfer transfer) {
         TransferDto transferDto;

        transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotif(transfer.getMotifTransfer());

        return transferDto;

    }



    public  Transfer toTransfer(TransferDto transferDto){
        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setMontantTransfer(transferDto.getMontant());
        return transfer;
    }
}
