package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;

public class DepositMapper {


    public DepositDto toDepositDto(MoneyDeposit deposit){
        DepositDto dto = new DepositDto();
        dto.setMontant(deposit.getMontant());
        dto.setMotif(deposit.getMotifDeposit());
        dto.setNomPrenomEmetteur(deposit.getNomPrenomEmetteur());
        return dto;
    }


}
