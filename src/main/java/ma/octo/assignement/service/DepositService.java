package ma.octo.assignement.service;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;

import java.util.List;


public interface DepositService {

    List<MoneyDeposit> findAll();

    void createDeposit(DepositDto deposit) throws DepositException, CompteNonExistantException;
}
