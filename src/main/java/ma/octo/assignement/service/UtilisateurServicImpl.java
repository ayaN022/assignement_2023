package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UtilisateurServicImpl implements UtilisateurService{


    private final UtilisateurRepository utilisateurRepository;

    @Autowired
    public UtilisateurServicImpl(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }

    @Override
    public List<Utilisateur> findAllUsers() {
        return utilisateurRepository.findAll();
    }
}
