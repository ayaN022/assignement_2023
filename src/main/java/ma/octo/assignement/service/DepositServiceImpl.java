package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
public class DepositServiceImpl implements DepositService{
    public static final int MONTANT_MAXIMAL = 10000;

    private final DepositRepository depositRepository;

    private final CompteRepository compteRepository;

    private final AuditService auditService;

    public DepositServiceImpl(DepositRepository depositRepository, CompteRepository compteRepository, AuditService auditService) {
        this.depositRepository = depositRepository;
        this.compteRepository = compteRepository;
        this.auditService = auditService;
    }

    @Override
    public List<MoneyDeposit> findAll() {
        return depositRepository.findAll();
    }



    @Override
    public void createDeposit(DepositDto depositDto) throws DepositException, CompteNonExistantException {
        Compte compteBeneficiaire = compteRepository.findCompteByRib(depositDto.getRib());
        String msg;

        if(depositDto.getRib() == null){
            msg="Rib vide";
            System.out.println(msg);
            throw new DepositException(msg);
        }else if (compteBeneficiaire == null) {
            msg="Compte Non existant";
            System.out.println(msg);
            throw new CompteNonExistantException(msg);
        }else if (depositDto.getMontant() == null) {
            System.out.println("Montant vide");
            throw new DepositException("Montant vide");
        }else if (depositDto.getMontant().intValue() == 0) {
            System.out.println("Montant vide");
            throw new DepositException("Montant vide");
        } else if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de deposit dépassé");
            throw new DepositException("Montant maximal de deposit dépassé");
        }

        if( depositDto.getNomPrenomEmetteur() == null ){
            System.out.println("Le nom d'emetteur est vide");
            throw new DepositException(("Emetteur Nom et Prenom vide"));
        }
        if (depositDto.getMotif() == null) {
            System.out.println("Motif vide");
            throw new DepositException("Motif vide");
        }

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() +  depositDto.getMontant().intValue()));
        compteRepository.save(compteBeneficiaire);

        MoneyDeposit deposit = new MoneyDeposit();
        deposit.setMontant(depositDto.getMontant());
        deposit.setCompteBeneficiaire(compteBeneficiaire);
        deposit.setMotifDeposit(depositDto.getMotif());
        deposit.setNomPrenomEmetteur(depositDto.getNomPrenomEmetteur());

        depositRepository.save(deposit);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime lstartDay= now.with(LocalTime.MIDNIGHT);
        LocalDateTime lendDy = now.plusDays(1);
        Date dayStart = Date.from(lstartDay.atZone(ZoneId.systemDefault()).toInstant());
        Date dayEnd = Date.from(lendDy.atZone(ZoneId.systemDefault()).toInstant());



        if(depositRepository.findNrOfDepositInADay(dayStart,dayEnd,compteBeneficiaire.getId())>11){
            System.out.println("ce compte a depasse la limite de nombre de versement");
        }


            auditService.auditDeposit("Deposit vers le compt de RIB"+ depositDto.getRib()+" par  "+ depositDto.getNomPrenomEmetteur());

    }
}
