package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;

@Service
public class TransferServiceImpl implements TransferService{

    private static final Logger LOGGER = LoggerFactory.getLogger(TransferService.class);

    public static final int MONTANT_MAXIMAL = 10000;

    private final CompteRepository compteRepository;

    private final TransferRepository transferRepository;

    private final AuditService monService;

    public TransferServiceImpl(CompteRepository compteRepository, TransferRepository transferRepository, AuditService monService) {
        this.compteRepository = compteRepository;
        this.transferRepository = transferRepository;
        this.monService = monService;
    }

    @Override
    public List<Transfer> findAllTransfers() {
        return transferRepository.findAll();
    }

    public Transfer saveEmployee(Transfer transfer) {
        return transferRepository.save(transfer);
    }

    @Override
    public void transferMoney(TransferDto transferDto) throws TransactionException, CompteNonExistantException {

        Compte compteEmetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null || compteBeneficiaire == null) {
            LOGGER.error("Compte Non existant");
           throw new CompteNonExistantException("Compte Non existant");
        }else if (transferDto.getMontant()== null || transferDto.getMontant().intValue() == 0) {
            LOGGER.error("Montant vide");
           throw new TransactionException("Montant vide");

        } else if (transferDto.getMontant().intValue() < 10) {
            LOGGER.error("Montant minimal de transfer non atteint");
           throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de transfer dépassé");


           throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (compteEmetteur.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }

        if (compteEmetteur.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire
                .setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + transferDto.getMontant().intValue()));
        compteRepository.save(compteBeneficiaire);



        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
         transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMontantTransfer(transferDto.getMontant());

        transferRepository.save(transfer);

        monService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());

    }
}
