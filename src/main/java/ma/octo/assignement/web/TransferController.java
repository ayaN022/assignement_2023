package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.TransferService;
import ma.octo.assignement.service.UtilisateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/transfers")
class TransferController {
    private static final  Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    private final TransferService transferService;
    private final CompteService compteService;
    private final UtilisateurService utilisateurService;

    public TransferController(TransferService transferService, CompteService compteService, UtilisateurService utilisateurService) {
        this.transferService = transferService;
        this.compteService = compteService;
        this.utilisateurService = utilisateurService;
    }

    @GetMapping("listDesTransferts")
    public List<Transfer> loadAllTransfers() {
        LOGGER.info("Lister des utilisateurs");
        return transferService.findAllTransfers();
    }


    @GetMapping("listOfAccounts")
    public List<Compte> loadAllCompte() {
        LOGGER.info("Lister des comptes");
        return compteService.findAllAccounts();

    }


    @GetMapping("lister_utilisateurs")
    public List<Utilisateur> loadAllUtilisateur() {
       return  utilisateurService.findAllUsers();


    }


    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws  CompteNonExistantException, TransactionException {

      transferService.transferMoney(transferDto);

    }

}
