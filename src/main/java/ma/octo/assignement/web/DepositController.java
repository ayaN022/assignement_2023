package ma.octo.assignement.web;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/deposit")
public class DepositController {


    private final DepositService depositService;

    @Autowired
    public DepositController(DepositService depositService) {
        this.depositService = depositService;
    }

    @GetMapping("depositList")
    public List<MoneyDeposit> loadAllTransfers() {
        return depositService.findAll();
    }

    @PostMapping("/executerDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit(@RequestBody DepositDto depositDto) throws CompteNonExistantException, DepositException {
        depositService.createDeposit(depositDto);
    }
}
