package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface DepositRepository extends JpaRepository<MoneyDeposit, Long> {


  @Query(value = "select count(a) from MoneyDeposit a, Compte c where c.id = ?1 and  a.dateExecution BETWEEN :dayStart and  :dayEnd ")
   int findNrOfDepositInADay(@Param("dayStart") Date dayStart,@Param("dayEnd") Date dayEnd, long compteId );



}
